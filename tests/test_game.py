"""Tests for the game"""
import pytest

from game.game import Board, Orientation, OutOfBoundsError

class TestGame:

    def test_new_board(self):
        """Tests that a new board is correctly created with a the piece in
        position 0 0 N and a size of 5
        """
        board = Board()
        assert board.size == 5
        assert board.piece.pos.x == 0
        assert board.piece.pos.y == 0
        assert board.piece.pos.orientation == Orientation.North

    def test_forward_one(self):
        """Tests that the piece can be moved 1 position forward from the start
        position resulting in 0 1 N
        """
        sequence = "M"
        board = Board()

        new_position = board.move(sequence)

        assert new_position.x == 0
        assert new_position.y == 1
        assert new_position.orientation == Orientation.North
        assert str(new_position) == "0 1 N"

    def test_rotate_right_one(self):
        """Tests that the piece can be rotated 1 position to right from the
        start position resulting in 0 0 E
        """
        sequence = "R"
        board = Board()
        new_position = board.move(sequence)

        assert new_position.x == 0
        assert new_position.y == 0
        assert new_position.orientation == Orientation.East
        assert str(new_position) == "0 0 E"

    def test_rotate_left_one(self):
        """Tests that the piece can be rotated 1 position to left from the
        start position resulting in 0 0 W
        """
        sequence = "L"
        board = Board()
        new_position = board.move(sequence)

        assert new_position.x == 0
        assert new_position.y == 0
        assert new_position.orientation == Orientation.West
        assert str(new_position) == "0 0 W"

    def test_rotate_right_one_full_rotation(self):
        """Tests that the piece can be rotated all the way to the right back to
        start position 0 0 N
        """
        sequence = "RRRR"
        board = Board()
        new_position = board.move(sequence)

        assert new_position.x == 0
        assert new_position.y == 0
        assert new_position.orientation == Orientation.North
        assert str(new_position) == "0 0 N"

    def test_rotate_left_one_full_rotation(self):
        """Tests that the piece can be rotated all the way to the left back to
        start position 0 0 N
        """
        sequence = "LLLL"
        board = Board()
        new_position = board.move(sequence)

        assert new_position.x == 0
        assert new_position.y == 0
        assert new_position.orientation == Orientation.North
        assert str(new_position) == "0 0 N"

    def test_full_board_traversal(self):
        """Tests that a piece can traverse the whole board to 4 4 N
        """
        sequence = "MRMLMRMLMRMLMRML"
        board = Board()
        new_position = board.move(sequence)

        assert new_position.x == 4
        assert new_position.y == 4
        assert new_position.orientation == Orientation.North
        assert str(new_position) == "4 4 N"

    def test_out_of_bounds_north(self):
        """Tests that an OutOfBoundsError is raised if the pieces moves
        out of bounds North
        """
        sequence = "MMMMM"
        board = Board()

        with pytest.raises(OutOfBoundsError):
            board.move(sequence)

        assert board.piece.pos.x == 0
        assert board.piece.pos.y == 0
        assert board.piece.pos.orientation == Orientation.North

    def test_out_of_bounds_east(self):
        """Tests that an OutOfBoundsError is raised if the pieces moves
        out of bounds East
        """
        sequence = "RMMMMM"
        board = Board()

        with pytest.raises(OutOfBoundsError):
            board.move(sequence)

        assert board.piece.pos.x == 0
        assert board.piece.pos.y == 0
        assert board.piece.pos.orientation == Orientation.North

    def test_out_of_bounds_south(self):
        """Tests that an OutOfBoundsError is raised if the pieces moves
        out of bounds South
        """
        sequence = "RRMMMMM"
        board = Board()

        with pytest.raises(OutOfBoundsError):
            board.move(sequence)

        assert board.piece.pos.x == 0
        assert board.piece.pos.y == 0
        assert board.piece.pos.orientation == Orientation.North

    def test_out_of_bounds_west(self):
        """Tests that an OutOfBoundsError is raised if the pieces moves
        out of bounds West
        """
        sequence = "RRRMMMMM"
        board = Board()

        with pytest.raises(OutOfBoundsError):
            board.move(sequence)

        assert board.piece.pos.x == 0
        assert board.piece.pos.y == 0
        assert board.piece.pos.orientation == Orientation.North