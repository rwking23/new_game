# Game
The game package includes a single module with a Class representing the board and a Class representing a piece. To move the piece around the board the move method on the board should be provided a valid sequence.

## Running The Tests

A Dockerfile is provided that can be used to run the tests

1. build the image using `docker build -t game_tests .`
2. run the tests using `docker run game_tests`

Tests can also be run locally by running `pip install -r requirements.txt` followed by `pytest tests/test_game.py` this requires python 3.7 to be installed locally.