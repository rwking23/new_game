FROM python:3.7.4-alpine3.10

ADD . /lib

RUN pip install -r /lib/requirements.txt

WORKDIR /lib

ENTRYPOINT ["pytest", "tests/test_game.py"]