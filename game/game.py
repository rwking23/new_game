
from copy import deepcopy
from enum import Enum

class Orientation(Enum):
    """Enum for representing compass directions"""
    North = 1
    East = 2
    South = 3
    West = 4

class Position:
    """Represents a position on the board. Always initialised to 0,0 N"""
    def __init__(self):
        self.x = 0
        self.y = 0
        self.orientation =  Orientation.North
        self.orientation_mapping = {
            Orientation.North: "N",
            Orientation.East: "E",
            Orientation.South: "S",
            Orientation.West: "W"
        }

    def __str__(self):
        return f"{self.x} {self.y} {self.orientation_mapping[self.orientation]}"

class OutOfBoundsError(Exception):
    """Error raised when a piece if out of bounds"""

class Board:
    """Class for representing the board for the game"""

    def __init__(self, size=5):
        self.size = size
        self.piece = Piece()
        
    def move(self, sequence):
        """Moves the piece along the provided sequence. Sequence is expected to
        include the following characters

        M - Move forward
        R - Turn right
        L - Turn left

        Assuming for now the provided sequence always has valid characters

        Parameters:
            sequence (string): The sequence to move the piece by
        Returns:
            tuple: Of position (x,y) and orientation
        Raises:
            OutOfBoundsError
        """
        # Get the current position so we can reset the piece if required
        position = deepcopy(self.piece.position())
        newPosition = self.piece.move(sequence)

        if newPosition.x >= self.size or newPosition.y >= self.size:
            self.piece.set_position(position)
            raise OutOfBoundsError("Out of bounds of the board")

        if newPosition.x < 0 or newPosition.y < 0:
            self.piece.set_position(position)
            raise OutOfBoundsError("Out of bounds of the board")

        return newPosition


class Piece:
    """Class representing a piece on the board"""

    def __init__(self):
        self.pos = Position()

    def move(self, sequence):
        """Moves the piece along the sequence provided returning the new 
        position when done
        
        Parameters:
            sequence (string): The sequence to move the piece by
        Returns:
            Class: The new position
        """
        for instruction in sequence:
            if instruction == "M":
                self.__forward()
            elif instruction == "R":
                self.__right()
            elif instruction == "L":
                self.__left()
            else:
                print("invalid instruction")

        return self.pos

    def position(self):
        """Current position of the piece on the board
        
        Returns
            Class: Representing the current position
        """
        return self.pos

    def set_position(self, position):
        """Sets the pieces position to the provided position
        
        Parameters:
            position (class): New position
        """
        self.pos = position

    def __forward(self):
        """Moves the piece forward in the direction of the current orientation
        by 1 position
        """
        if self.pos.orientation == Orientation.North: 
            # Moving North y should be incremented
            self.pos.y += 1
        elif self.pos.orientation == Orientation.South:
            # Moving South y should be decremented
            self.pos.y -= 1
        elif self.pos.orientation == Orientation.East:
            # Moving East x should be incremented
            self.pos.x += 1
        elif self.pos.orientation == Orientation.West:
            # Moving West x should be decremented
            self.pos.x -= 1
        else:
            print("Shouldn't happen")

    def __right(self):
        """Rotates the piece 1 orientation to the right"""

        # Incrementing this would result in an invalid orientation so need to
        # set it to North
        if self.pos.orientation == Orientation.West:
            self.pos.orientation = Orientation.North
            return
        self.pos.orientation = Orientation(self.pos.orientation.value + 1)

    def __left(self):
        """Rotates the piece 1 orientation to the left"""
        # Decrementing this would result in an invalid orientation so need to
        # set it to West
        if self.pos.orientation == Orientation.North:
            self.pos.orientation = Orientation.West
            return
        self.pos.orientation = Orientation(self.pos.orientation.value - 1)